package de.awacademy.bsp1;

/**
 * Execptions:
 * ein nicht geplantes Ereignis,
 * das während der Ausführung eines Programms vorkommt und dessen normalen Ablauf stört
 * -----> führt zu Programmabsturz
 */

public class Main {
    public static void main(String[] args) {

        System.out.println(div(1, 0));
    }

    static double div(int number1, int number2) throws ArithmeticException {// <---- "throws.." ist hier optional (Java Exception)

        try {
            return number1 / number2;
        }                                    // Bsp.: Execptions, die von Java geworfen werden:
        catch (ArithmeticException ex) {     // −Division durch 0 ( ArithmeticException )
                                             // −Lesen über Arraygrenzen hinweg ( IndexOutOfBoundsException )
            return -1;                       // −Lesen über das Ende einer Datei hinaus ( EOFException )
        }
        /*
        .
        catch..     <----  mehrere "catch" Anweisungen sind möglich
        .
        catch..
        .
         */
    }
}