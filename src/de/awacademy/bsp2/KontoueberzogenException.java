/**
 *
 * In dieser Klasse wird die Ausnahme definiert, Schlagwort "extends Exception" *
 *
 */

package de.awacademy.bsp2;

public class  KontoueberzogenException extends Exception {

    public KontoueberzogenException(double kontoStand) // durch das Auslösen der Ausnahme wird ein Objekt konstruiert
    {
        System.out.println(kontoStand + " Du darfst dein Konto nicht ueberziehen");
    }
}