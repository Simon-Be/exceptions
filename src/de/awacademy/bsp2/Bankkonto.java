/**
 * In dieser Klasse wird die Methode definiert, welche die Ausnahme werfen soll
 */

package de.awacademy.bsp2;

class Bankkonto {

    private double kontoStand = 1000;

    public double abheben(double betrag) throws KontoueberzogenException {
        if (this.kontoStand - betrag >= 0) {

            return kontoStand - betrag;

        } else {

            throw new KontoueberzogenException(this.kontoStand);

        }
    }
}